import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product: Product = new Product();
    product.price = createProductDto.price;
    product.name = createProductDto.name;

    return this.productRepository.save(product);
  }

  findAll() {
    return this.productRepository.find;
  }

  findOne(id: number) {
    return this.productRepository.findBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productRepository.findOneBy({ id: id });
    const updateProduct = { ...product, ...updateProductDto };
    return this.productRepository.save(updateProduct);
  }

  async remove(id: number) {
    const product = await this.productRepository.findOneBy({ id: id });
    return this.productRepository.remove(product);
  }
}
